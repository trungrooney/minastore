<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="q29-IxmlIuYA92rLEVqK049JKrsnlIqXuW9saJymbkw">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'nileforest_wrapper_before_body' ); ?>
<!-- Newsletter Popup -->
<?php if(theme_get_option('popup_btn') != 'no'):	
	get_template_part( 'template-parts/header/header', 'popup' );
endif; ?>
<!-- End Newsletter Popup -->	
<!-- Sidebar Menu (Cart Menu) -->
<?php if ( class_exists( 'WooCommerce' ) ) : ?>
	<?php if(theme_get_option('header_addtocart_btn') == 'yes'):		
		get_template_part( 'template-parts/header/header', 'cartpopup' );
	endif; ?>
<?php endif; ?>
<!-- End Sidebar Menu (Cart Menu) -->
<!-- Search Overlay Menu -->
<?php
	if(theme_get_option('header_search_btn') == 'yes'):		
		get_template_part( 'template-parts/header/header', 'search' );
	endif;
?>
<!-- End Search Overlay Menu -->
<div id="page" class="wraper">	
	<?php 
		$custom_header = get_custom_header_markup(); 
    	if ( !empty( $custom_header ) ):
		?>
		<div class="custom-header">
			<div class="custom-header-media">
				<?php the_custom_header_markup(); ?>
			</div>
		</div><!-- .custom-header -->
		<?php endif; ?>
	<div id="top-bar" class="header-top nav-dark">
		<div class="flex-row container">
			<div class="flex-col show-for-medium flex-left">
				<strong class="uppercase">Trải nghiệm cảm giác ăn vặt</strong>
			</div>

			<div class="flex-col hide-for-medium flex-right">
				<ul class="nav top-bar-nav nav-right nav-small  nav-divided">
					<li id="menu-item-169" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-169"><a href="#" class="nav-top-link">Hướng dẫn đặt hàng tại MiNaStoreHCM</a></li>
					<li id="menu-item-170" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-170"><a href="#" class="nav-top-link">Hướng dẫn thanh toán</a></li>
					<li id="menu-item-171" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-171"><a href="<?php echo get_site_url(); ?>/my-account/" class="nav-top-link">Đăng nhập</a></li>
					<li id="menu-item-171" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-171"><a href="#" class="nav-top-link">Liên hệ</a></li>
					<li class="html header-social-icons ml-0">
						<div class="social-icons follow-icons ">
							<a href="http://url" target="_blank" data-label="Facebook" rel="nofollow" class="icon plain facebook tooltip tooltipstered"><i class="icon-facebook"></i></a>
							<a href="http://url" target="_blank" rel="nofollow" data-label="Instagram" class="icon plain  instagram tooltip tooltipstered"><i class="icon-instagram"></i></a>
							<a href="http://url" target="_blank" data-label="Twitter" rel="nofollow" class="icon plain  twitter tooltip tooltipstered"><i class="icon-twitter"></i></a>
							<a href="mailto:your@email" data-label="E-mail" rel="nofollow" class="icon plain  email tooltip tooltipstered"><i class="icon-envelop"></i></a>
						</div>
					</li>         
				</ul>
			</div>		
		</div>
	</div>
	<header class="header">
		<?php get_template_part( 'template-parts/header/header', 'image' ); ?>
	</header><!-- #masthead -->	
	<div class="site-content-contain">
		<div id="content" class="page-content-wraper">
