<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Nileforest
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->
		<?php if(theme_get_option('footer_widget') != 'no'): ?>
		<footer class="footer" role="contentinfo">			
				<?php
				get_template_part( 'template-parts/footer/footer', 'widgets' );				

				get_template_part( 'template-parts/footer/site', 'info' );
				?>		
		</footer><!-- #colophon -->
		<?php endif; ?>
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php do_action( 'nileforest_end_wrapper_container' ); ?>
<?php wp_footer(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-20344846-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-20344846-2');
</script>
<div class="social-media">

	<div id="phones">
		<div class="yami-alo-ph-circle"></div>
		<div class="yami-alo-ph-circle-fill"></div>
		<a href="tel:0902557993">
			<div class="yami-phones-ph-img-circle yami-alo-ph-circle-shake"></div>
		</a>
	</div>
	<div class="face-message item-contact ">
			<a class="face " target="_blank" rel="nofollow" href="https://m.me/MiNaStoreHCM">
				<i class="icon icon_messenger"></i>
				<div class="title">Nhắn tin qua facebook</div>
			</a>
		</div>
</div>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/5e9c761669e9320caac53dfc/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
