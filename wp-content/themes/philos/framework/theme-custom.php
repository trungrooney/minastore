<?php 
function filter_media_comment_status( $open, $post_id ) {
    $post = get_post( $post_id );
    if( $post->post_type == 'attachment' ) {
        return false;
    }
    return false;
}
add_filter( 'comments_open', 'filter_media_comment_status', 10 , 2 );
?>

<?php
//Theme Tech
function theme_tech_custom_post(){
    $labels = array(
       'name'                           => _x('Tech','techs','trung-framework'),
       'singular_name'                  => _x('Tech','tech','trung-framework'),
       'add_new' 						 => _x('Add New', 'team member','trung-framework'),
       'add_new_item'                   => __('Add New Tech','trung-framework'),
       'search_items'                   => __('Search Techs','trung-framework'),
       'all_items'                      => __('All Techs','trung-framework'),
       'edit_item'                      => __('Edit Tech','trung-framework'),
       'update_item'                    => __('Update Tech','trung-framework'),
       'new_item_name'                  => __('New Tech Name','trung-framework'),
       'menu_name'                      => __('Tech','trung-framework'),
       'view_item'                      => __('View Tech','trung-framework'),
       'popular_items'                  => __('Popular Tech','trung-framework'),
       'separate_items_with_commas'     => __('Separate team with commas','trung-framework'),
       'add_or_remove_items'            => __('Add or remove techs','trung-framework'),
       'choose_from_most_used'          => __('Choose from the most used techs','trung-framework'),
       'not_found'                      => __('No techs found','trung-framework')
   );

   $args = array(
       'labels' 				=> $labels,
       'public' 			 	=> true,
       'publicly_queryable' 	=> true,
       'show_ui' 				=> true,
       'query_var' 			    => true,
       'rewrite' 				=> array('slug'=>'tech'),
       'capability_type' 		=> 'post',
       'menu_position' 		    => null,
       'menu_icon' 			    => 'dashicons-groups',
       'supports' 				=> array('title','editor','author','thumbnail'),
     );
     // The following is the main step where we register the post.
     register_post_type('tech',$args);
    }
//hook function
add_filter('init','theme_tech_custom_post'); 

?>