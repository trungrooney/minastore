<?php
/**
 * Template part for displaying posts
 * Template Name: Contact us
 */

get_header(); 
?>
<?php /* include TEMPLATEPATH . '/includes/commons/client-quotes.php'; */?>
<div class="container">
<?php
$args = array(
    'post_type' => 'tech', // Your custom post type
    'posts_per_page' => '8', // Change the number to whatever you wish
    'order_by' => 'date', // Some optional sorting
    'order' => 'ASC', 
    );
    $new_query = new WP_Query ($args);
    if ($new_query->have_posts()) {
        while($new_query->have_posts()){
            $new_query->the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 

            $excerpt = get_the_excerpt();            
            $excerpt = substr($excerpt, 0, 260);
            $result = substr($excerpt, 0, strrpos($excerpt, ' '));
            ?>

            <div class="item">
                <div class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                <div class="images"><?php the_post_thumbnail('array(500, 500)');?> </div>
                <div class="feature_images"><?php $featured_img_url ?> </div>
                <div class="content"><?php the_content(); ?></div> 
                <div class="expert"><?php echo $result; ?></div>
                <div class="link_readmore"><a href="<?php the_permalink(); ?>"><?php echo esc_html( 'Read more' )?></a></div>
            </div>
            <?php
            // Get a list of post's categories
            $categories = get_the_category($post->ID);
            foreach ($categories as $category) {
                echo $category->name;
            }
        }
    }
    wp_reset_postdata();
?>
</div>

<?php get_footer();