<?php
function tp_custom_checkout_fields( $fields ) {
    // Ẩn mã bưu chính
    unset( $fields['postcode'] );
    
    // Ẩn địa chỉ thứ hai
    unset( $fields['address_2'] );
    
    // Đổi tên Bang / Hạt thành Tỉnh / Thành
    $fields['state']['label'] = 'Tỉnh / Thành';
    
    // Đổi tên Tỉnh / Thành phố thành Quận / Huyện
    $fields['city']['label'] = 'Quận / Huyện';
    
    return $fields;
    }
    add_filter( 'woocommerce_default_address_fields', 'tp_custom_checkout_fields' );

add_filter( 'woocommerce_states', 'vietnam_cities_woocommerce' );
function vietnam_cities_woocommerce( $states ) {
$states['VN'] = array(
'CANTHO' => __('Cần Thơ', 'woocommerce') ,
'HCM' => __('Hồ Chí Minh', 'woocommerce') ,
'HANOI' => __('Hà Nội', 'woocommerce') ,
'HAIPHONG' => __('Hải Phòng', 'woocommerce') ,
'DANANG' => __('Đà Nẵng', 'woocommerce') ,
'ANGIAG' => __('An Giang', 'woocommerce') ,
'BRVT' => __('Bà Rịa - Vũng Tàu', 'woocommerce') ,
'BALIE' => __('Bạc Liêu', 'woocommerce') ,
'BACKAN' => __('Bắc Kạn', 'woocommerce') ,
'BACNINH' => __('Bắc Ninh', 'woocommerce') ,
'BACGIANG' => __('Bắc Giang', 'woocommerce') ,
'BENTRE' => __('Bến Tre', 'woocommerce') ,
'BDUONG' => __('Bình Dương', 'woocommerce') ,
'BDINH' => __('Bình Định', 'woocommerce') ,
'BPHUOC' => __('Bình Phước', 'woocommerce') ,
'BTHUAN' => __('Bình Thuận', 'woocommerce'),
'CAMAU' => __('Cà Mau', 'woocommerce'),
'DAKLAK' => __('Đak Lak', 'woocommerce'),
'DAKNONG' => __('Đak Nông', 'woocommerce'),
'DIENBIEN' => __('Điện Biên', 'woocommerce'),
'ĐNAI' => __('Đồng Nai', 'woocommerce'),
'GIALAI' => __('Gia Lai', 'woocommerce'),
'HGIANG' => __('Hà Giang', 'woocommerce'),
'HNAM' => __('Hà Nam', 'woocommerce'),
'HTINH' => __('Hà Tĩnh', 'woocommerce'),
'HDUONG' => __('Hải Dương', 'woocommerce'),
'HUGIANG' => __('Hậu Giang', 'woocommerce'),
'HOABINH' => __('Hòa Bình', 'woocommerce'),
'HYEN' => __('Hưng Yên', 'woocommerce'),
'KHOA' => __('Khánh Hòa', 'woocommerce'),
'KGIANG' => __('Kiên Giang', 'woocommerce'),
'KTUM' => __('Kom Tum', 'woocommerce'),
'LCHAU' => __('Lai Châu', 'woocommerce'),
'LAMDONG' => __('Lâm Đồng', 'woocommerce'),
'LSON' => __('Lạng Sơn', 'woocommerce'),
'LCAI' => __('Lào Cai', 'woocommerce'),
'LAN' => __('Long An', 'woocommerce'),
'NDINH' => __('Nam Định', 'woocommerce'),
'NGAN' => __('Nghệ An', 'woocommerce'),
'NBINH' => __('Ninh Bình', 'woocommerce'),
'NTHUAN' => __('Ninh Thuận', 'woocommerce'),
'PTHO' => __('Phú Thọ', 'woocommerce'),
'PYEN' => __('Phú Yên', 'woocommerce'),
'QBINH' => __('Quảng Bình', 'woocommerce'),
'QNAM' => __('Quảng Nam', 'woocommerce'),
'QNGAI' => __('Quảng Ngãi', 'woocommerce'),
'QNINH' => __('Quảng Ninh', 'woocommerce'),
'QTRI' => __('Quảng Trị', 'woocommerce'),
'STRANG' => __('Sóc Trăng', 'woocommerce'),
'SLA' => __('Sơn La', 'woocommerce'),
'TNINH' => __('Tây Ninh', 'woocommerce'),
'TBINH' => __('Thái Bình', 'woocommerce'),
'TNGUYEN' => __('Thái Nguyên', 'woocommerce'),
'THOA' => __('Thanh Hóa', 'woocommerce'),
'TTHIEN' => __('Thừa Thiên - Huế', 'woocommerce'),
'TGIANG' => __('Tiền Giang', 'woocommerce'),
'TVINH' => __('Trà Vinh', 'woocommerce'),
'TQUANG' => __('Tuyên Quang', 'woocommerce'),
'VLONG' => __('Vĩnh Long', 'woocommerce'),
'VPHUC' => __('Vĩnh Phúc', 'woocommerce'),
'YBAI' => __('Yên Bái', 'woocommerce'),
);

return $states;
}

?>