<?php
// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
    $fields['order']['order_comments']['placeholder'] = 'Ghi chú của bạn';
	unset($fields['billing']['billing_postcode']);
	unset($fields['billing']['billing_country']);
	// unset($fields['billing']['billing_city']);
	// unset($fields['billing']['billing_email']);
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_city']);
    unset($fields['shipping']['shipping_postcode']);
    unset($fields['shipping']['shipping_country']);
    unset($fields['shipping']['shipping_state']);
	unset($fields['shipping']['shipping_last_name']);
    return $fields;
}

// add_filter( 'woocommerce_billing_fields', 'custom_override_billing_fields' );
// function custom_override_billing_fields( $fields ) {
//     $fields['billing_address_1']['required'] = false;
// 	unset($fields['billing_last_name']);
//     return $fields;
// }


//Remove all file css of woocommerce 
// add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

?>