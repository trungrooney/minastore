<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
	<div class="content-page">
		<?php if(theme_get_option('signle_page_sidebar') != 'no-sidebar'): ?>
			<div class="container">
				<div class="row">
					<div class="<?php echo(theme_get_option('signle_page_sidebar') == 'left-sidebar' ? "col-md-9 push-md-3" : "col-md-9");?>">
						<?php while ( have_posts() ) : the_post(); ?>

						<?php wc_get_template_part( 'content', 'single-product' ); ?>
			
						<?php endwhile; // end of the loop. ?>
					</div>
					<div class="sidebar-container<?php echo(theme_get_option('signle_page_sidebar') == 'left-sidebar' ? " col-md-3 pull-md-9" : " col-md-3");?>">
						<div class="sidebar-shopinfo">
							<div class="sidebar-shopinfo-top">
								<div class="current-seller">									
									<div class="name">
										<div class="img"></div>
										<div class="text">MiNaStore HCM</div>
										<div class="text-small">Cam kết chất lượng 100%</div>
									</div>
								
									<div class="warranty-info">
										<div class="img"></div>
										<div class="text">
											<span>Giao dịch an toàn</span>
											<div class="text-small">Bạn hoàn toàn chủ động</div>
										</div>
									</div>
								</div>
							</div>
							<div class="sidebar-shopinfo-bottom">
								<div class="additional">
									<div class="item">
										<div class="img"></div>
										<p>
											<b>Liên hệ</b><br>
											HOTLINE: <a href="tel:+84902557993">0902 557 993</a><br>
											<small>( 8-21h cả T7, CN)</small>
										</p>
									</div>
									<div class="item email">
										<div class="img"></div>
										<p>
											Email: <a href="mailto:info@binhgolf.com">info.minastore@gmail.com</a>
										</p>
									</div>
								</div>
							</div>
						</div>
						<?php
							/**
							 * woocommerce_sidebar hook.
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							do_action( 'woocommerce_sidebar' );
						?>
					</div>
				</div>
			</div>
		<?php else:	 ?>
			<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

			<?php endwhile; // end of the loop. ?>
		<?php endif; ?>		
	</div>
	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

<?php get_footer( 'shop' );
/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */